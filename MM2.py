'''
    Instituto Tecnologico de Costa Rica
    Escuela de Ingenieria en Computacion
    
    Curso: Inteligencia Artificial
    Programa: Mastermind
    Archivo:  MM2.py
 
    Estudiantes: 
            Adrian Rodriguez Villalobos     201021837
            Ilenia Vasquez Vasquez     
            Genesis Salazar Barquero     
            
    Profesor:
            Eddy Ramirez Jimenez
    
    Fecha: 15/03/2015
'''
import time
import struct
import socket
import itertools
import random

def dec(byte):
    return struct.unpack('B', byte)[0]

def swap2(c, fijos):
    i=0
    j=0
    x=0
    while fijos.count(x)!=0:
        x+=1
    i=x
    x=0
    while fijos.count(x)!=0 or x==i:
        x+=1
    j=x
    c = list(c)
    c[i], c[j] = c[j], c[i]
    return [''.join(c), 0, fijos, 0]

def swap(c, i, j):
    c = list(c)
    c[i], c[j] = c[j], c[i]
    return ''.join(c)

def nextGuess(R, N, Ra, Na, g, p, k, fijos, sA):
    if R+N == k:
        return swap2(g, fijos)
    if R+N == 0:
        return [g.replace(str(p-1), str(p)), sA, fijos, 1]
    elif R+N >= 1:
        if Na > N:
            fijos.append(sA)
        if N > Na or (Na == N and N!=0):
            d= N - Na
            i=0
            while d >= 0:
                if g[i] == str(p-2):
                    su = 1
                    while fijos.count(i+su)!=0:
                        su+=1
                    g = swap(g, i, i+su)
                    sA = i
                    d-=1
                i+=1
            return [g, sA, fijos, 0]
        d = k-(R+N)
        i = k-1
        while d > 0:
            if g[i]== str(p-1):
                g = g[:i]+str(p)+g[(i+1):]
                d-=1
            i-=1
        return [g, sA, fijos, 1]
                    
    

def newGuess(k):
    guess = ""
    while len(guess) < k:
        guess+= "0"
    return guess

print("Id?")
myid = input()
#En este caso el id m'aximo es f(15). Si tiene que ser mayor a f, hay que
#cambiar el bit de 0 en *** (es hexadecimal)
host = 'localhost'
port = 8000
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
s.send(bytes.fromhex('0'+myid))#***
n = dec(s.recv(1))
k = dec(s.recv(1))
print ('Received: N = %d K = %d' % (n,k))

while n != 0 and k != 0:
    R = 0
    N = 0
    Ra = 0
    Na = 0
    g=""
    p=0
    fijos = []
    sA=0
    while True:
        if p==0:
            guess = newGuess(k)
            g=guess
            print(guess)
            p+=1
        else:
            ans = nextGuess(R, N, Ra, Na, g, p, k, fijos, sA)
            guess = ans[0]
            fijos = ans[2]
            sA = ans[1]
            g=guess
            print(guess)
            if ans[3]==1:
                p+=1
        for i in range(k):
            s.send(bytes.fromhex('0'+guess[i]))
        Ra = R
        Na = N
        R = dec(s.recv(1))
        N = dec(s.recv(1))
        print ('Received: R = %d B = %d' % (R, N))
        if R == 255:
            print("I lost.")
            break
        if R == k:
            print("I won!")
            break
    n = dec(s.recv(1))
    k = dec(s.recv(1))
    Ra = 0
    Na = 0
    g=""
    p=0
    fijos = []
    sA=0
    print ('Received: N = %d K = %d' % (n,k))
    if n == 0:
        print("All test cases processed!")
            

s.close()


    
    
