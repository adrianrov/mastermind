import time
import struct
import socket

def dec(byte):
    return struct.unpack('B', byte)[0]

print("Id?")
myid = input()
#En este caso el id m'aximo es f(15). Si tiene que ser mayor a f, hay que
#cambiar el bit de 0 en *** (es hexadecimal)
host = 'localhost'
port = 8000
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
s.send(bytes.fromhex('0'+myid))#***
n = dec(s.recv(1))
k = dec(s.recv(1))
print ('Received: N = %d K = %d' % (n,k))
while n != 0 and k != 0:
    R = 0
    N = 0
    while True:
        guess=input()
        for i in range(k):
            s.send(bytes.fromhex('0'+guess[i]))
        R = dec(s.recv(1))
        N = dec(s.recv(1))
        print ('Received: R = %d N = %d' % (R, N))
        if R == 255:
            print("I lost.")
            break
        if R == k:
            print("I won!")
            break
    n = dec(s.recv(1))
    k = dec(s.recv(1))
    print ('Received: N = %d K = %d' % (n,k))
    if n == 0:
        print("All test cases processed!")
            

s.close()
